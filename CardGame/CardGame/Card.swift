//
//  Card.swift
//  CardGame
//
//  Created by manalogo on 8/30/1399 AP.
//

import Foundation

struct Card {
    var isFaceUp = false
    var isMatch = false
    var id : Int
    //var state = false
    
    static var idFactory=0
    static func generateNewId(idFactory :  Int) -> Int
    {
        Card.idFactory += 1
        return idFactory
    }
    
    init(){
        self.id = Card.generateNewId(idFactory: Card.idFactory)
    }
    
    
}
