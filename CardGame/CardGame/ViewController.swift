//
//  ViewController.swift
//  CardGame
//
//  Created by manalogo on 8/30/1399 AP.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var StartGameBut: UIButton!
    
    //****Class GameManager
    lazy var gameManager = GameManager(numberOfPairedCard: (cardsButton.count + 1)/2)
    
    @IBOutlet var cardsButton: [UIButton]!
    
    //***Count Of MachCards ... to repeat Game!
    var matchCount = 0
    
    //***StartGameButton
    @IBAction func startGame(_ sender: UIButton) {
        
        if StartGameBut.isEnabled {
            //***Enabled StartGameButton
            sender.isEnabled = false
            sender.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            //***To set CardButtons
            setCardButtons(sender)
            
        }else{
            //***Disable StartGameButton
            sender.isEnabled = true
            sender.backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            //***To set CardButtons
            setCardButtons(sender)
        }
    }
    
    
    //***set CardButtons
    func setCardButtons(_ sender : UIButton) {
        
        //*** if StartGame Button is Disable
        if !sender.isEnabled {
            
            //***All CardButtons are Enabled and Back CradButtonns
            
            for i in 0..<cardsButton.count{
                cardsButton[i].isEnabled = true
                cardsButton[i].backgroundColor = #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
                cardsButton[i].isHidden=false
            }
            
        }else
        {
            for i in 0..<cardsButton.count{
                cardsButton[i].isEnabled = false
                cardsButton[i].isHidden=true
            }
        }
    }
    
    
    @IBAction func cardTouch(_ sender: UIButton) {
        if let index = cardsButton.firstIndex(of: sender)
        {
            gameManager.chooseCard(at: index)
            updateUI()
        }
        else {
            print("button is not in array")
        }
    }
    
    func updateUI() {
        //***Counter for 2Card
        var counter = 0
        for index in cardsButton.indices {
            let  button = cardsButton[index]
            var  card = gameManager.cards[index]
            
            //***if FrontCardButto
            if card.isFaceUp{
                
                //***If FrontCardButtons and IsmatchCards
                button.setTitle(emoji(at: card), for: .normal)
                button.backgroundColor = .white
                counter += 1
                //***If 2Cards is The same & 2Cards isFaceUp & 2Cards isMatch
                
                //***2CardButton Transparent!
                if counter == 2  , card.isFaceUp , card.isMatch{
                    //count of matchCount CardButtns!
                    matchCount += 1
                    print("(\(matchCount)")
                    
                    //***Use Timer To Fade Action of CardButtons
                    Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { [self] timer in
                        
                        //***For Search of 2 CardButtons Is Same
                        for indexnew in cardsButton.indices {
                            
                            let buttonNew = cardsButton[indexnew]
                            let  cardNew = gameManager.cards[indexnew]
                            //***
                            if cardNew.isMatch , cardNew.isFaceUp {
                                buttonNew.setTitle("", for: .normal)
                                buttonNew.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
                               
                            }
                            else{
                               
                                
                                //false isMache to donot transparent cardsButton
                                card.isMatch = false
                                card.isFaceUp = false
                                
                                
                                // if 2card not found! count set to ziro
                                counter = 0
                                
                                
                            }
                        }
                        //***Timer Is Off
                        timer.invalidate()
                        
                        if (matchCount == (cardsButton.count + 1)/2){
                            //reload GameManager & emojies Array & reset matchCount
                            reload()
                        }
                    }
                }
            }
            //***If BackCardButtons
            else {
                print("\(card.isMatch) \(card.isFaceUp) ----")
                button.setTitle("", for: .normal)
                button.backgroundColor = card.isMatch ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) : #colorLiteral(red: 1, green: 0.5781051517, blue: 0, alpha: 1)
            }
        }
    }
    
    var emojies = ["😅","😍","🤪","🥰","😎","😡"]
    var emojiDictionary = [Int: String]()
    func emoji(at card: Card) -> String {
        if emojiDictionary[card.id] == nil,
           emojies.count > 0 {
            let randomIndex = Int.random(in: emojies.indices)
            let emoji = emojies.remove(at: randomIndex)
            emojiDictionary[card.id] = emoji
        }
        return emojiDictionary[card.id] ?? "?"
    }
    
    func reload(){
        
        //***Load New Game
        startGame(StartGameBut)
        
        //Untile 2CardButtons the Same is equall to cardsButton.count + 1)/2){
        matchCount = 0
        
        //Reload Class GameManager and reset Card.isMath & Card.isFaceUp & Card.id !
        gameManager = GameManager(numberOfPairedCard: (cardsButton.count + 1)/2)
        
        //Because Remove emojies Array for Create Random CardButtons
        emojies = ["😅","😍","🤪","🥰","😎","😡"]
    }
}




